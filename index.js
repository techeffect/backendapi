require('dotenv').config()
const axios = require('axios');


const apiKey = process.env.BACKEND_API_KEY;
const backendURL = process.env.BACKEND_URL;
const MOON = process.env.MOON;

class BackendApiClient {

    async getConfigBySession(session) {

        const projectId = await this.getProjectId(session);
        if (projectId === null) {
            return false;
        }
        const moonConfig = await this.getMoonConfig(projectId);

        if (moonConfig === null) {
            return false;
        }

        return moonConfig;
    }

    async getMoonConfig(botId) {
        const response = await axios.get(backendURL + '/api/bot/' + botId, {
            params: {
                'api-key': apiKey
            }
        });
        if (!response.data) {
            return null;
        }
        const data = response.data;
        if (response.status !== 200) {
            return null;
        }
        return data.secret.plugins[MOON];
    }

    async getProjectId(project_id) {
        const response = await axios.get(backendURL + '/api/bot-id/' + project_id, {
            params: {
                'api-key': apiKey
            }
        });

        if (!response.data) {
            return null;
        }
        const data = response.data;
        if (response.status !== 200) {
            return null;
        }
        return data.bot_id;
    }
}

module.exports.BackendApiClient = BackendApiClient;